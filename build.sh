#!/bin/bash

ZEEBE_VERSION="0.25.3"
HAZELCAST_EXPORTER_VERSION="0.10.0"

IMAGE_NAME="zeebe"
REPOSITORY="nikolaypervukhin"

rm ./zeebe-container/target -R
mkdir ./zeebe-container/target
cd ./zeebe-container/target
wget -nv --no-check-certificate https://github.com/zeebe-io/zeebe/releases/download/$ZEEBE_VERSION/zeebe-distribution-$ZEEBE_VERSION.zip
unzip zeebe-distribution-$ZEEBE_VERSION.zip
rm zeebe-distribution-$ZEEBE_VERSION.zip
mv zeebe-broker-$ZEEBE_VERSION zeebe-broker
cd ../..

# Hazelcast exporter
mkdir  ./zeebe-container/target/zeebe-broker/exporters
cd  ./zeebe-container/target/zeebe-broker/exporters
wget -nv --no-check-certificate https://github.com/zeebe-io/zeebe-hazelcast-exporter/releases/download/$HAZELCAST_EXPORTER_VERSION/zeebe-hazelcast-exporter-$HAZELCAST_EXPORTER_VERSION-jar-with-dependencies.jar
cd ../..
cat ../config.yml >> ./zeebe-broker/config/application.yaml
cd ../..

docker build -t $IMAGE_NAME ./zeebe-container
docker tag $IMAGE_NAME $REPOSITORY/$IMAGE_NAME
docker login --username $DOCKER_LOGIN --password $DOCKER_PASSWORD
docker push $REPOSITORY/$IMAGE_NAME